<!-- MODULE Home categories -->
<div class="home_categories clearfix">
    {if isset($categories) AND $categories}
            <ul>
            {foreach from=$categories item=category name=homeCategories}
                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
                    <li class="ajax_block_category {if $smarty.foreach.homeCategories.first}first_item{elseif $smarty.foreach.homeCategories.last}last_item{else}item{/if}">
                        <a href="{$categoryLink}" title="{$category.name}" class="category_image"><img src="{$img_cat_dir}{$category.id_category}-square.jpg" alt="{$category.name}" title="{$category.name}" class="categoryImage" /></a>                    
                   </li>
            {/foreach}
            </ul>
    {else}
        <p>{l s='No categories' mod='homecategories'}</p>
  {/if}
</div>
<!-- /MODULE Home categories -->