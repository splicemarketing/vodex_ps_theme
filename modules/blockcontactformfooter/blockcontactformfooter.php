<?php
/*
* 2014 Pascal van Geest
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Pascal van Geest <info@ModulesModules.com>
*  @copyright  2014 Pascal van Geest / www.ModulesModules.com

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
class Blockcontactformfooter extends Module
{
	public function __construct()
	{
		$this->name = 'blockcontactformfooter';
		if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
			$this->tab = 'front_office_features';
		else
			$this->tab = 'Blocks';
		$this->version = '1.0';

		$this->author = 'Pascal van Geest / www.ModulesModules.com';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Block contact form footer');
		$this->description = $this->l('This module adds a contact form in the page footer.');
	}
	
	public function install()
	{
		return (parent::install() 
				&& $this->registerHook('header') && $this->registerHook('footer'));
	}


	public function uninstall()
	{
		if (!parent::uninstall() ||
				!$this->unregisterHook('header') ||
				!$this->unregisterHook('footer'))
			return false;
		return true;
	}
	
	public function hookHeader()
	{
		$this->context->controller->addCSS(($this->_path).'blockcontactformfooter.css', 'all');
	}
	
	public function hookFooter($params)
	{	
		if (!$this->isCached('blockcontactformfooter.tpl', $this->getCacheId()))
		return $this->display(__FILE__, 'blockcontactformfooter.tpl', $this->getCacheId());
	}
}
?>
