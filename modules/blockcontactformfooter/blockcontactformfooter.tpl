{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Pascal van Geest <contact@ModulesModules.com>
*  @copyright  2014 Pascal van Geest

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Block contact infos -->
{if $page_name != 'contact' }
	<div id="block_contact_form_footer">
		<h4 class="title_block">{l s='Send a message' mod='blockcontactformfooter'}</h4>
		<h6>Please complete the details below</h6>
		<form action="/contact-us" method="post" class="std" enctype="multipart/form-data">
			<fieldset>
				<div class="form-group">
					<input class="form-control" type="text" placeholder="Name" id="name" name="name" value="">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" placeholder="Email" id="email" name="from" value="">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" placeholder="Telephone" id="tek" name="tel" value="">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" placeholder="Subject" id="subject" name="subject" value="">
				</div>				
				<div class="form-group">
					<textarea class="form-control" id="message" placeholder="Message" name="message"></textarea>
				</div>
				<div class="submit">
					<input type="submit" name="submitMessage" id="submitMessage" value="Send" class="button_large">
				</div>
			</fieldset>
		</form>
	</div>
{/if}

<!-- /MODULE Block contact infos -->
