<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{relatedfree}prestashop>relatedfree_17c82299e6df09fdb6d988f6bcbd69e3'] = 'Podobne produkty (darmowy)';
$_MODULE['<{relatedfree}prestashop>relatedfree_6eb29cbec7ae39124375af6321fe620b'] = 'Darmowy moduł do wyświetlania podobnych produktów na stronach wybranych produktów';
$_MODULE['<{relatedfree}prestashop>relatedfree_ec211f7c20af43e742bf2570c3cb84f9'] = 'Dodaj';
$_MODULE['<{relatedfree}prestashop>relatedfree_b9af8635591dc44009ccd8e5389722ec'] = 'Nie znaleziono produktów';
$_MODULE['<{relatedfree}prestashop>relatedfree_248336101b461380a4b2391a7625493d'] = 'Zapisz';
$_MODULE['<{relatedfree}prestashop>relatedfree_9a51a007b33a46e553def6423aad8648'] = 'Globalne ustawienia';
$_MODULE['<{relatedfree}prestashop>relatedfree_c9ae5a4214e87ad6fbed44a267471eee'] = 'zapisz zmiany';
$_MODULE['<{relatedfree}prestashop>tabs_7ac24d6ee168c8d39992c0c59171fd92'] = 'Chcesz więcej opcji wyświetlania?';
$_MODULE['<{relatedfree}prestashop>tabs_34749ebdcabd53ce9171aefc837f71ec'] = 'Chcesz wyświetlić produkty w dowolnej kolejności?';
$_MODULE['<{relatedfree}prestashop>tabs_339517f38f0df46f9e4283b34a65d412'] = 'Chcesz wybrać konkretne produkty do wyświetlenia?';
$_MODULE['<{relatedfree}prestashop>tabs_12bf350cb48f3fa45af36abb5860b37d'] = 'Chcesz dodać podobne produkty w formie zakładek?';
$_MODULE['<{relatedfree}prestashop>tabs_fe6c1af2438c25b00cbb6400da54f6a1'] = 'Chcesz utworzyć dowolną ilość bloków / zakładek ?';
$_MODULE['<{relatedfree}prestashop>tabs_824c644d076c03565a2a5aabdc0957bc'] = 'Chcesz najlepsze rozwiązanie do tworzenia \"podobnych\" produktów?';
$_MODULE['<{relatedfree}prestashop>tabs_0ba4439ee9a46d9d9f14c60f88f45f87'] = 'Sprawdź';
$_MODULE['<{relatedfree}prestashop>tabs_22884db148f0ffb0d830ba431102b0b5'] = 'Moduł';
$_MODULE['<{relatedfree}prestashop>tabs_846b75ee07b94396c75ec27d5b58fd9e'] = 'Listy podobnych produktów';
$_MODULE['<{relatedfree}prestashop>tabs_f9edc7482d3eff492319e6c033e9cf55'] = 'ID kategori';
$_MODULE['<{relatedfree}prestashop>tabs_7415647ac5fbdc4f5946d36d7a81cca4'] = 'skąd wziąć ID kategorii?';
$_MODULE['<{relatedfree}prestashop>tabs_990fc90e39367377c314f1d61522ae61'] = 'Liczba produktów';
$_MODULE['<{relatedfree}prestashop>tabs_9ea67be453eaccf020697b4654fc021a'] = 'Zapisz zmiany';
$_MODULE['<{relatedfree}prestashop>products16_38070661d5ad384d9c7d21895dc4e784'] = 'Podobne produkty';
$_MODULE['<{relatedfree}prestashop>products16_1a8c0228da8fb16885881e6c97b96f08'] = 'Brak produktów';
